<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
      echo "Jika nilai 98 : ".tentukan_nilai(98) . "<br/>";
      echo "Jika nilai 76 : ".tentukan_nilai(76) . "<br/>";
      echo "Jika nilai 67 : ".tentukan_nilai(67) . "<br/>";
      echo "Jika nilai 43 : ".tentukan_nilai(43) . "<br/>";

      function tentukan_nilai($nilai)
      {
          if($nilai > 85 && $nilai < 100)
          {
              return "Sangat Baik";
          }
          else if($nilai > 70 && $nilai < 85)
          {
            return "Baik";
          }
          else if($nilai > 60 && $nilai < 70)
          {
            return "Cukup";
          }
          else
          {
            return "Kurang";
          }
      }
    ?>
</body>
</html>