<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    $Kids = array("Mike","Dustin","Will","Lucas","Max","Eleven");
    $Adults = array("Hopper","Nancy","Joyce","Jonathan","Murray");
    ?>

    <table>
        <tr>
            <td><h1>Kids</h1></td>
        </tr>
        <?php
            $count = count($Kids);
            for($i=0; $i < $count; $i++){
        ?>
        <tr>
            <td><?php echo $Kids[$i]?></td>
        </tr>
        <?php
        }
        ?>

    </table>
<hr>
    <table>
        <tr>
        <td><h1>Adults</h1></td>
        </tr>
        <?php
            $count = count($Adults);
            for($i=0; $i < $count; $i++){
        ?>
        <tr>
            <td><?php echo $Adults[$i]?></td>
        </tr>
        <?php
        }
        ?>

    </table>
</body>
</html>